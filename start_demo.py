from flask import Flask, render_template, request, jsonify
import threading
import uuid
from ImageProcessorModule.ImageProcessor import ImageProcessor


app = Flask ("Morphology Operations Demo", template_folder = "templates", static_folder = "static")

@app.route("/")
def main_view():
	return render_template("morphology.html")

@app.route("/handle-post-data", methods=["POST"])
def handle_post_data():
	if request.method == "POST":
		data = request.get_json(silent=True)

		morphology_type = int(data["morphologyType"])
		kernel_size = (int(data["kernelWidth"]), int(data["kernelHeight"]))
		kernel_shape = int(data["kernelShape"])
		iterations = int(data["numOfIterations"])

		output = process_image(data["inputImage"], morphology_type, kernel_shape, kernel_size, iterations)
		 

		return jsonify({"outputImage": output})


def process_image(img_uri, morphology_type, kernel_shape, kernel_size, iterations):
	ip = ImageProcessor(img_uri)
	ip.data_uri_to_cv2_img()
	return ip.run_morphology(morphology_type, kernel_shape, kernel_size, iterations)

if __name__ == "__main__":
	app.run ()
