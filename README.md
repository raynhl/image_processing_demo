# Morphology Operations Interactive Application - [Live Demo Here](https://guarded-sands-96489.herokuapp.com/)

Mathematical morphology is a crucial step in image processing that is widely used to extract information about forms and shapes of structures. This web application aims to demonstrate the operations of morphology by utilizing the Flask framework and openCV on Python. The project was developed and aimed to run on Linux-based machine (tested on Ubuntu 16.04).

Project is also hosted on Heroku. Check it out [HERE](https://guarded-sands-96489.herokuapp.com/).

## Getting Started


### Installing dependency

From your terminal, install Flask and openCV

    pip3 install -r requirements.txt

### Running the application


To get the web server running, simply run the following command from the root folder.

    gunicorn start_demo:app
    
## Accessing the content


### Adding the custom package index

Fire up your internet browser (Google Chrome recommended) and enter the following address in the address bar:

    http://127.0.0.1:8000/


## Playing around with the web application


At the bottom of the webpage, there is an interactive application for user to try different morphological operations on an input image. Generally, it requires users to:

    1. Select input image.
	2. Select a morphological operation.
	3. Select kernel shape.
	4. Select kernel size.
	5. Choose number of iterations.
	6. Hit the run button.

A final image that has undergone the morphological operation will be displayed. 


## Disclaimer

This is a project part of my class at University Sains Malaysia.

Tested on the following system: Ubuntu 16.04, Google Chrome, Python3 
