function resizeImage(imgSrc) {
	var img = new Image();

	img.onload = function() {
		var MAX_WIDTH = 640;
		var MAX_HEIGHT = 480;
		var width = img.width;
		var height = img.height;

		if (width > height) {
			if (width > MAX_WIDTH) {
				height *= MAX_WIDTH / width;
				width = MAX_WIDTH;
			}
		} else {
			if (height > MAX_HEIGHT) {
				width *= MAX_HEIGHT / height;
				height = MAX_HEIGHT;
			}
		}

		var canvas = document.createElement("canvas");
		var ctx = canvas.getContext("2d");
		canvas.width = width;
		canvas.height = height;
		ctx.drawImage(img, 0, 0, width, height);

		var preview = document.getElementById("previewImage");
		preview.width = canvas.width
		preview.height = canvas.height
		preview.src = canvas.toDataURL("image/jpeg", 1.0);
	};

	img.src = imgSrc;
}

window.onload=function() {
	document.getElementById("fileInput").addEventListener("change",
	function() {
		var file = this.files[0];
		var reader = new FileReader();

		reader.onload = function() {
			imgUrl = reader.result;
			resizeImage(imgUrl);
		};

		reader.readAsDataURL(file);

	}, false);

	document.getElementById("widthSlider").addEventListener("change",
	function() {
		var output = document.getElementById("kernelWidth");
		output.innerHTML = this.value;
	}, false);

	document.getElementById("heightSlider").addEventListener("change",
	function() {
		var output = document.getElementById("kernelHeight");
		output.innerHTML = this.value;
	}, false);

	document.getElementById("iterationSlider").addEventListener("change",
	function() {
		var output = document.getElementById("numOfIterations");
		output.innerHTML = this.value;
	}, false);
};


function submitForm() {
	console.log("[RAY] Form submitted")

	var preview = document.getElementById("previewImage");
	var morphologyTypes = document.getElementsByName("morphology_type");
	var kernelShapes = document.getElementsByName("kernel_shape");
	var kernelWidthSlider = document.getElementById("widthSlider");
	var kernelHeightSlider = document.getElementById("heightSlider");
	
	var morphologyType;
	var kernelShape;
	var kernelWidth = kernelWidthSlider.value;
	var kernelHeight = kernelHeightSlider.value;

	for (var i = 0; i < morphologyTypes.length; i++) {
		if (morphologyTypes[i].checked) {
			morphologyType = morphologyTypes[i].value;
		}
	}

	for (var i = 0; i < kernelShapes.length; i++) {
		if (kernelShapes[i].checked) {
			kernelShape = kernelShapes[i].value;
		}
	}

	if (morphologyType == 3 | morphologyType == 4) {
		var iterationSlider = document.getElementById("iterationSlider");
		var output = document.getElementById("numOfIterations");
		iterationSlider.value = 1;
		output.innerHTML = iterationSlider.value;
	}

	var jsonData = {
		"morphologyType": morphologyType,
		"kernelShape": kernelShape,
		"kernelWidth": kernelWidth,
		"kernelHeight": kernelHeight,		
		"numOfIterations": document.getElementById("iterationSlider").value,
		"inputImage": document.getElementById("previewImage").src
	};

	var str = ""
	
	switch(jsonData["morphologyType"]) {
		case "1":
			str += "Erosion; ";
			break;
		case "2":
			str += "Dilation; ";
			break;
		case "3":
			str += "Opening; ";
			break;
		case "4":
			str += "Closing; ";
			break;
	}

	switch(jsonData["kernelShape"]) {
		case "1":
			str += "Rectangle; ";
			break;
		case "2":
			str += "Ellipse; ";
			break;
		case "3":
			str += "Cross; ";
			break;
	}


	str += "Kernel Size: (" + jsonData["kernelWidth"] + ", " + jsonData["kernelHeight"] + "); ";
	str += "Iterations: " + jsonData["numOfIterations"];

	console.log(jsonData);

	var xmlHttp = new XMLHttpRequest();   // new HttpRequest instance 
	xmlHttp.open("POST", "/handle-post-data", true);
	xmlHttp.setRequestHeader("Content-Type", "application/json");

	// process when data is returned
	xmlHttp.onreadystatechange = function() {
		var obj = JSON.parse(this.response);
		var output = document.getElementById("outputImage");
		output.width = preview.width;
		output.height = preview.height;
		output.src = obj["outputImage"];

		document.getElementById("outputDescription").innerHTML = str;
	};

	xmlHttp.send(JSON.stringify(jsonData));
}


