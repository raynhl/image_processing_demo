import cv2
import numpy as np
import time
import base64

class ImageProcessor():
	def __init__(self, input_image = None):
		self.input_image = input_image
		self.KERNEL_SHAPES = {
			1: cv2.MORPH_RECT,
			2: cv2.MORPH_ELLIPSE,
			3: cv2.MORPH_CROSS,
		}
		self.MORPHOLOGY_TYPE = {
			1: self.erode,
			2: self.dilate,
			3: self.opening,
			4: self.closing	
		}

		self.base64_prefix = ""

	def set_input_image(self, path):
		self.input_image = path

	def erode(self, kernel_shape, kernel_size, iterations):
		kernel = cv2.getStructuringElement(self.KERNEL_SHAPES[kernel_shape], kernel_size)
		erosion = cv2.erode(self.input_image, kernel, iterations = iterations)
		return erosion

	def dilate(self, kernel_shape, kernel_size, iterations):
		kernel = cv2.getStructuringElement(self.KERNEL_SHAPES[kernel_shape], kernel_size)
		dilation = cv2.dilate(self.input_image, kernel_size, iterations = iterations)
		return dilation

	def opening(self, kernel_shape, kernel_size, iterations):
		kernel = cv2.getStructuringElement(self.KERNEL_SHAPES[kernel_shape], kernel_size)
		opening = cv2.morphologyEx(self.input_image, cv2.MORPH_OPEN, kernel)
		return opening

	def closing(self, kernel_shape, kernel_size, iterations):
		kernel = cv2.getStructuringElement(self.KERNEL_SHAPES[kernel_shape], kernel_size)
		closing = cv2.morphologyEx(self.input_image, cv2.MORPH_CLOSE, kernel)
		return closing

	def show_image(self, img, window_name):
		cv2.imshow(window_name, img)
		cv2.waitKey(0)
		cv2.destroyAllWindows()

	def data_uri_to_cv2_img(self):
		encoded_data = self.input_image.split(",")[1]
		self.base64_prefix = self.input_image.split(",")[0]
		nparr = np.fromstring(base64.b64decode(encoded_data), np.uint8)
		img = cv2.imdecode(nparr, cv2.IMREAD_COLOR)
		self.input_image = img

	def run_morphology(self, morphology_type, kernel_shape, kernel_size, iterations):
		processed_image = self.MORPHOLOGY_TYPE[morphology_type](kernel_shape, kernel_size, iterations)
		jpeg_processed_image = cv2.imencode(".jpg", processed_image)[1].tostring()
		return self.base64_prefix + "," + base64.b64encode(jpeg_processed_image).decode("utf-8")
